# **s.co-op**

****Wir bieten als Genossenschaft eine Netzwerk aus Kreativschaffenden in dem wir Geld-, Arbeits- und Zeitersparnisse erreichen, weil auf Wissen und Ressourcen zurückgegriffen werden kann.
Wir bieten uns und der Industrie leichten Zugang zu Arbeitsgruppen mit breit gefächerten Expertisen die kreative, originelle und neue Ideen entwickeln.
Außerdem haben wir ein Netzwerk aus Firmen, Gestaltern und Betrieben mit denen wir in Kontakt stehen.
Dadurch haben wir Credibility, Manpower, Bekanntheit und PR-Möglichkeiten.
Projektpetrischale, 
Durch Standardisierung der Dokumentation, Qualität und Preise können wir leicht Vermittlung an passende Kompetenzen betreiben und fungieren als Schnittstelle, Übergang zwischen Hochschule und Arbeitsmarkt.****

## **Inhalt** 
[Wer sind wir?](#wersindwir)  
[Was tun wir?](#wastunwir)  
[Wer kann mitmachen?](#werkannmitmachen)  
[Besonders suchen wir aber Leute für uns fremde Bereiche](#fremdebereiche)  
[Wie kann ich mitmachen?](#wiekannichmitmachen)  
[Was brauchen wir, wie du helfen kannst](#wasbrauchenwir)  

## **Wer sind wir?** <a name="wersindwir">

Wir sind eine vernetzte Gemeinschaft aus Kreativschaffenden Studenten und Alumni die aus der Not eines strukturierten Übergangs vom Studium in den Arbeitsmarkt entstanden ist und seinen Ursprung an der HBK Saar hat.

## **Was tun wir?** <a name="wastunwir">

![Was wir tun](http://wordpress.p514803.webspaceconfig.de/wp-content/uploads/2019/05/Kopie-von-s.coop-Plakat_hbk_web-724x1024.jpg)

**Problem**
* Wer weiß was und kann mir helfen?
* Wo kann ich meine Projekte entwickeln?
* Wem kann ich bei Projekten helfen wenn mir Arbeit fehlt?
* Wer arbeitet mit mir an großen Projekten? 
* Hatten Andere bereits ähnliche Probleme und Lösungen?
* Wo kann man arbeiten und wo finde ich das passende Werkzeug?
* Alleine fehlt es an Glaubwürdigkeit und Mitsprachemöglichkeiten.
* Firmen suchen kreative und innovative Ideen, wissen aber nicht wo sie diese finden.


**Lösung**
* Social Network 
* Netzwerk mit themenspezifischen Tags (Wer weiß was?)
* FabLab / HUB (eigene Produktionsmöglichkeiten)
* als Gruppe können auch große Aufgaben bewältigt werden
* klare Kommunikationskanäle für Externe und Interessierte bieten



## **Wer kann mitmachen?** <a name="werkannmitmachen">
 
* Gestalter und Kreativschaffende aus allen Bereichen

**Besonders suchen wir aber Leute für uns fremde Bereiche:** <a name="fremdebereiche">

* Programmierer 
* Produktdesigner 
* Juristen 
* Betriebswissenschaftler
* Ingenieure
* Architekten


## **Wie kann ich mitmachen?** <a name="wiekannichmitmachen">

Schau dir mal unsere Website an
http://s.co-op.works/

Schreib uns einfach unter [hi@s.co-op.works
](mailto)

Komm zu einem unserer regelmäßigen Treffen Donnerstags ab 16 Uhr in der Game Lounge des eHaus der HBK um uns perönlich zu treffen.



Wir wollen die Welt zusammen besser gestalten. 

**Unser Arbeitsplatz: Digital & Offline**
+ Wöchentliches Treffen
+ GoogleDocs
+ FabLab 
+ Projektmanagement-Plattform 
+ GitLab 
+ Social Media


## **Was brauchen wir, wie du helfen kannst** <a name="wasbrauchenwir">

Werkzeuge, Plattform mit Teammitgliedern und Skillbeschreibung, Software, OpenSource Community, Geeignete Räumlichkeiten, Projektmanagement Plattform, Knowledge Base (für Formulare, Produktentwicklung etc., FAQ für Neueinsteiger), Diversität der Mitglieder, Auftragsverteilungsmanagmentsystem
Information/Expertise  zu Buchhaltung, Genossenschaftsrecht, Projektmanagment, Auftragsverteilungmanagment


Unsere Website
http://s.co-op.works/

## **Partner**

https://gitlab.com/projektgarten/projektgarten
http://www.k8.design/_/
https://dpz.xmlab.org/dpz-home

